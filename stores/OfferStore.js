import GeoFire from 'geofire'
import moment from 'moment'
import Firebase from 'firebase'


const API = 'https://rofr-production-db.firebaseio.com';
const RADIUS = 200

let _offers = {}
let _initCalled = false
let _changeListeners = []
let _watchLocationRef = null

let _geoQuery = null
let _geoFire = null

const OfferStore = {
	init: () => {
		if (_initCalled)
		return
		console.log('init');
		let fb = new Firebase(`${API}/locations`)
		_geoFire = new GeoFire(fb)
		_initCalled = true
		if (navigator.geolocation) {
			// navigator.geolocation.getCurrentPosition(geo_success, geo_error)
			_watchLocationRef = navigator.geolocation.watchPosition(geo_success, geo_error)
		}
	},
	getOffers: () => {
		const array = []

	    for (const id in _offers)
	      array.push(_offers[id])

	    return array
	},
	getOffer: (id) => {
		return new Promise((resolve, reject) => {
			if (_offers[id]) {
				resolve(_offers[id]);
			} else {
				let ref = new Firebase(`${API}/offers/${moment().format('YYYY-MM-DD')}/${id}`)
	  			ref.once('value', (snap) => {
	  				resolve(snap.val());
	  			});	
			}

			
		});
	},
	getStore: (name) => {
		return new Promise((resolve, reject) => {
			let ref = new Firebase(`${API}/store`)
			ref.once('value', (snap) => {
				const stores = snap.val()
				for (let storeId in stores) {
					if (stores[storeId].storeName === name) {
						resolve(stores[storeId]);
						return;		
					}
				}
				resolve(null);
			});	
		});
	},
	notifyChange: () => {
		_changeListeners.forEach(function (listener) {
	      listener()
	    })
	},
	addChangeListener: (listener) => {
	    _changeListeners.push(listener)
	},

	removeChangeListener: (listener) => {
		if (_watchLocationRef) {
			navigator.geolocation.clearWatch(_watchLocationRef)
		}
		if (_geoQuery) {
			_geoQuery.cancel()
			_geoQuery = null
		}
		if (_geoFire) {
			_geoFire = null
		}
	    _changeListeners = _changeListeners.filter(function (l) {
	    	return listener !== l
	    })
	    _initCalled = false
	}
}

var geoQueryReady = () => {
	// console.log("GeoQuery has loaded and fired all other events for initial data");
}
var geoQueryKeyEnter = (key, location, distance) => {
	// console.log(key + " entered query at " + location + " (" + distance + " km from center)");
		OfferStore.getOffer(key).then((offer) => {
			if (offer) {
				_offers[key]=offer;
				OfferStore.notifyChange()
			}
		})
}
var geo_success = (position) => {
	if (!_geoQuery) {
		_geoQuery = _geoFire.query({
			  center: [position.coords.latitude, position.coords.longitude],
			  radius: RADIUS
		});	
		_geoQuery.on("ready", geoQueryReady);
		_geoQuery.on("key_entered", geoQueryKeyEnter);
	} else {
		_geoQuery.updateCriteria({ 
  			center: [position.coords.latitude, position.coords.longitude]
  		});
	}
}
var geo_error = () => {
	console.log('Location not shared');
}

export default OfferStore