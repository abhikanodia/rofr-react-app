import React from 'react';
import NavLink from './NavLink'
import OfferStore from '../stores/OfferStore'
export default React.createClass({

    componentWillMount() {
        // OfferStore.init();
    },
    render() {
        return (
        	<div>
                <ul role="nav">
                    <li><NavLink to="/">Home</NavLink></li>
                    <li><NavLink to="/offers">Offers</NavLink></li>
                    <li><NavLink to="/store">Store</NavLink></li>
                </ul>
                {this.props.children}
        	</div>
        )
    }
})

