import React from 'react'
import { Router, Route, browserHistory, IndexRedirect } from 'react-router'
import App from './App'
import Home from './Home'
import Store from './Store'
import Offers from './Offers'
import OfferDetail from './OfferDetail'
import Cert from './Cert'

module.exports = (
  <Route path="/" component={App}>
    <IndexRedirect to="/offres" />
    <Route path="/stores" component={Store}>
    	<Route path="/stores/:storeName" component={Store} />
    </Route>
    <Route path="/offers" component={Offers} >
    	<Route path="/offers/:offerId" component={OfferDetail} />
    </Route>
  </Route>
)