import React from 'react';
import { Router, Link } from 'react-router';
import OfferStore from '../stores/OfferStore'
import DocumentMeta from 'react-document-meta';

const styles = {
	products: {
		display: 'flex',
  		flexWrap: 'wrap'
	},
	productCard: {
	  padding: '2%',
	  flexGrow: 1,
	  flexBasis: '16%'
	},
	productImage: {
		maxWidth: '100%'
	},
	productInfo: {
		marginTop: 'auto'
	}
};

export default React.createClass({
	contextTypes: {
	    router: React.PropTypes.object.isRequired
	},
	getInitialState() {
		return {
    		offers: [],
    		loading: true
    	};
  	},
  	componentWillMount() {
        
    },
	componentDidMount() {
		OfferStore.init();
		OfferStore.addChangeListener(this.updateOffers)
  	},
  	componentWillUnmount() {
	    OfferStore.removeChangeListener(this.updateOffers)
	},
  	updateOffers() {
  		if (!this.isMounted())
	      return

	    this.setState({
	      offers: OfferStore.getOffers(),
	      loading: false
	    })
  		
  	},
	render() {
		return (
			<div>
				<DocumentMeta extend/>
				<h1>Offers</h1>
				<div style={styles.products}>
				      {this.state.offers.map(offer => (
					   	<Link key={offer.id} to={'/offers/' + offer.id}>
					        <div style={styles.productCard}
					          key={offer.id}>
					          <div>
					          	<img style={styles.productImage} src={offer.photo} />
					          </div>
					          
					        </div>
				        </Link>
				      ))}
				</div>
				{this.props.children}
			</div>
		)
	}
});