import React from 'react';
import OfferStore from '../stores/OfferStore'
import { Router, Link } from 'react-router';
import DocumentMeta from 'react-document-meta';
import Favicon from 'react-favicon';

export default React.createClass({
  contextTypes: {
      router: React.PropTypes.object.isRequired
  },
	getInitialState() {
		return {
    		offer: {}
    	};
  	},
	  componentWillReceiveProps(nextProps) {
      if (!this.isMounted())
        return
      console.log(`componentWillReceiveProps ${nextProps.params.offerId}`);
      OfferStore.getOffer(nextProps.params.offerId).then((offer) => {
        this.setState({
          offer: offer
        });  
      })
    },
    // componentWillMount() {
    //   console.log('componentWillMount');
    //   if (!this.props.params.offerId) 
    //     return
    //   OfferStore.getOffer(this.props.params.offerId).then((offer) => {
    //     this.setState({
    //       offer: offer
    //     });  
    //   })
    // },
  	componentDidMount() {
      if (!this.isMounted())
        return
      console.log('componentDidMount');
      if (!this.props.params.offerId) 
        return
      OfferStore.getOffer(this.props.params.offerId).then((offer) => {
        this.setState({
          offer: offer
        });  
      })
  	},
	render() {
    const meta = {
        description: this.state.offer.shortDescription,
        title: this.state.offer.storeName
      }
		return (
			<div key={this.state.offer.id}>
        <DocumentMeta {...meta} extend/>
        <h2>Offer detail 1 of - {this.state.offer.id}</h2>
				<h3>{this.state.offer.description}</h3>
			</div>

		)
	}
})