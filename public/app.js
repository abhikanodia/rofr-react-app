if('serviceWorker' in navigator) {
  // navigator.serviceWorker
  //          .register('/sw.js')
  //          .then(function() { console.log('Service Worker Registered'); });
}
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
window.addEventListener("load", function load(event){
	var index = window.location.pathname.lastIndexOf('/')
	var storeName = getMetaContentByName('storeName')
	var shared = getParameterByName('shared');
  var type = getMetaContentByName('type')
	var description = getMetaContentByName('description')
  var parsedDescription = description.replace(/[%]+/g, "-percent").replace(/[\s]/g, "-").replace(/[.!,?#&\/]+/g, "");
  var parsedStoreName = storeName.toLowerCase().replace(/[%]+/g, "-percent").replace(/[\s]/g, "-").replace(/[.!,?#&\/]+/g, "");
  var queryString = '';
  if (type === 'offer') {
		if (shared)
    	queryString = 'utm_source=Shared&utm_medium='+shared+'&utm_term=Offer&utm_content='+parsedDescription+'&utm_campaign=Pilot'
		else {
			queryString = 'utm_source=Physical Web&utm_medium='+storeName+'&utm_term=Offer&utm_content='+parsedDescription+'&utm_campaign=Pilot'
		}
  } else if (type === 'store') {
		if (shared) {
			queryString = 'utm_source=Shared&utm_medium='+shared+'&utm_term=Storepage&utm_content=storepage'
		} else {
			queryString = 'utm_source=Physical Web&utm_medium='+storeName+'&utm_term=Storepage&utm_content=storepage'
		}
  }
  var descriptionParsed = parsedDescription;
  if (type === 'offer')
	 window.location="https://rofr.in/#!/offers/" + encodeURIComponent(descriptionParsed) +'?'+queryString;
  else if (type === 'store') {
    var storeType = getMetaContentByName('storeType')
    if (storeType === 'Store')
      window.location="https://rofr.in/#!/stores/" + encodeURIComponent(parsedStoreName) +'?'+queryString;
    else if (storeType === 'Hotel')
      window.location="https://rofr.in/#!/hotels/" + encodeURIComponent(parsedStoreName) +'?'+queryString;

  }



},false);


function getMetaContentByName(name,content){
   var content = (content==null)?'content':content;
   return document.querySelector("meta[name='"+name+"']").getAttribute(content);
}

function fixedEncodeURIComponent (str) {
  return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
    return '%' + c.charCodeAt(0).toString(16);
  });
}
