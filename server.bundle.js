/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__dirname) {'use strict';

	var _react = __webpack_require__(1);

	var _react2 = _interopRequireDefault(_react);

	var _server = __webpack_require__(2);

	var _reactRouter = __webpack_require__(3);

	var _routes = __webpack_require__(4);

	var _routes2 = _interopRequireDefault(_routes);

	var _OfferStore = __webpack_require__(7);

	var _OfferStore2 = _interopRequireDefault(_OfferStore);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var express = __webpack_require__(18);
	var path = __webpack_require__(19);
	var compression = __webpack_require__(20);

	var favicon = __webpack_require__(21);

	var app = express();
	app.use(compression());
	// serve our static stuff like index.css
	app.use(express.static(path.join(__dirname, 'node_modules/sw-toolbox')));
	app.use(express.static(path.join(__dirname, 'public')));

	// app.get('/.well-known/acme-challenge/:id', function(req, res) {
	//   res.send('ghV2hX0mn3xtOHpxs29a3G6hVRerNsBqA5HZYSMAjw4.d3amT42RzK2pDf4bzjhqCuhvWGRJCkXPmb3yuDRP-po');
	// });
	// send all requests to index.html so browserHistory in React Router works
	app.get('*', function (req, res) {
	  console.log('start');
	  (0, _reactRouter.match)({ routes: _routes2.default, location: req.url }, function (err, redirect, props) {
	    // in here we can make some decisions all at once
	    if (err) {
	      // there was an error somewhere during route matching
	      res.status(500).send(err.message);
	    } else if (redirect) {
	      console.log('redirect');
	      // we haven't talked about `onEnter` hooks on routes, but before a
	      // route is entered, it can redirect. Here we handle on the server.
	      res.redirect(redirect.pathname + redirect.search);
	    } else if (props) {
	      console.log(props.params);
	      if (props.params.offerId) {
	        _OfferStore2.default.getOffer(props.params.offerId).then(function (offer) {
	          if (!offer) {
	            res.status(404).send('Not Found');
	          } else {
	            // app.use(favicon(offer.storeLogo));
	            // if we got props then we matched a route and can render
	            var appHtml = (0, _server.renderToString)(_react2.default.createElement(_reactRouter.RouterContext, props));
	            res.send(renderPage(appHtml, offer));
	          }
	        });
	      } else if (props.params.storeName) {
	        _OfferStore2.default.getStore(props.params.storeName).then(function (store) {
	          if (!store) {
	            res.status(404).send('Not Found');
	          } else {
	            // app.use(favicon(offer.storeLogo));
	            // if we got props then we matched a route and can render
	            var appHtml = (0, _server.renderToString)(_react2.default.createElement(_reactRouter.RouterContext, props));
	            res.send(renderStorePage(appHtml, store));
	          }
	        });
	      } else {
	        // if we got props then we matched a route and can render
	        var appHtml = (0, _server.renderToString)(_react2.default.createElement(_reactRouter.RouterContext, props));
	        res.send(renderPage(appHtml, req.query.description));
	      }
	    } else {
	      //res.send(renderBlankPage())   
	      // no errors, no redirect, we just didn't match anything
	      res.status(404).send('Not Found');
	    }
	  });
	});
	function renderBlankPage() {
	  return '\n    <!DOCTYPE html>\n      <html>\n      <head>\n        <meta charset="utf-8">\n        <meta http-equiv="X-UA-Compatible" content="IE=edge">\n        <meta name="viewport" content="width=device-width, initial-scale=1.0">\n        <title>For cert</title>\n      </head>\n      <body>\n      </body>\n      </html>';
	}
	function renderPage(appHtml, _ref) {
	  var _ref$offerDescription = _ref.offerDescription;
	  var offerDescription = _ref$offerDescription === undefined ? 'Rofr' : _ref$offerDescription;
	  var _ref$storeName = _ref.storeName;
	  var storeName = _ref$storeName === undefined ? 'Rofr' : _ref$storeName;
	  var _ref$storeLogo = _ref.storeLogo;
	  var storeLogo = _ref$storeLogo === undefined ? 'favicon.ico' : _ref$storeLogo;

	  return '\n\t\t<!DOCTYPE html>\n  <html>\n  <head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge">\n    <meta name="viewport" content="width=device-width, initial-scale=1.0">\n    <meta name=\'description\' content=\'' + offerDescription + '\'>\n    <meta name=\'storeName\' content=\'' + storeName + '\'>\n    <meta name=\'type\' content=\'offer\'>\n    <title>' + storeName + '</title>\n    <link rel="shortcut icon" href=\'' + storeLogo + '\'>\n  </head>\n  <body>\n    <script src="/app.js"></script>\n  </body>\n  </html>';
	}

	function renderStorePage(appHtml, _ref2) {
	  var _ref2$storeMetaDescri = _ref2.storeMetaDescription;
	  var storeMetaDescription = _ref2$storeMetaDescri === undefined ? 'Rofr' : _ref2$storeMetaDescri;
	  var _ref2$storeMetaTitle = _ref2.storeMetaTitle;
	  var storeMetaTitle = _ref2$storeMetaTitle === undefined ? 'Rofr' : _ref2$storeMetaTitle;
	  var _ref2$storeName = _ref2.storeName;
	  var storeName = _ref2$storeName === undefined ? '' : _ref2$storeName;
	  var _ref2$storeType = _ref2.storeType;
	  var storeType = _ref2$storeType === undefined ? 'Store' : _ref2$storeType;

	  return '\n    <!DOCTYPE html>\n  <html>\n  <head>\n    <meta charset="utf-8">\n    <meta http-equiv="X-UA-Compatible" content="IE=edge">\n    <meta name="viewport" content="width=device-width, initial-scale=1.0">\n    <meta name=\'description\' content=\'' + storeMetaDescription + '\'>\n    <meta name=\'storeName\' content=\'' + storeName + '\'>\n    <meta name=\'type\' content=\'store\'>\n    <meta name=\'storeType\' content=\'' + storeType + '\'>\n    <title>' + storeMetaTitle + '</title>\n    <link rel="shortcut icon" href=\'favicon.ico\'>\n  </head>\n  <body>\n    <script src="/app.js"></script>\n  </body>\n  </html>';
	}

	var PORT = process.env.PORT || 8080;
	app.listen(PORT, function () {
	  console.log('Production Express server running at localhost:' + PORT);
	});
	/* WEBPACK VAR INJECTION */}.call(exports, ""))

/***/ },
/* 1 */
/***/ function(module, exports) {

	module.exports = require("react");

/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = require("react-dom/server");

/***/ },
/* 3 */
/***/ function(module, exports) {

	module.exports = require("react-router");

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _react = __webpack_require__(1);

	var _react2 = _interopRequireDefault(_react);

	var _reactRouter = __webpack_require__(3);

	var _App = __webpack_require__(5);

	var _App2 = _interopRequireDefault(_App);

	var _Home = __webpack_require__(11);

	var _Home2 = _interopRequireDefault(_Home);

	var _Store = __webpack_require__(12);

	var _Store2 = _interopRequireDefault(_Store);

	var _Offers = __webpack_require__(13);

	var _Offers2 = _interopRequireDefault(_Offers);

	var _OfferDetail = __webpack_require__(15);

	var _OfferDetail2 = _interopRequireDefault(_OfferDetail);

	var _Cert = __webpack_require__(17);

	var _Cert2 = _interopRequireDefault(_Cert);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	module.exports = _react2.default.createElement(
	  _reactRouter.Route,
	  { path: '/', component: _App2.default },
	  _react2.default.createElement(_reactRouter.IndexRedirect, { to: '/offres' }),
	  _react2.default.createElement(
	    _reactRouter.Route,
	    { path: '/stores', component: _Store2.default },
	    _react2.default.createElement(_reactRouter.Route, { path: '/stores/:storeName', component: _Store2.default })
	  ),
	  _react2.default.createElement(
	    _reactRouter.Route,
	    { path: '/offers', component: _Offers2.default },
	    _react2.default.createElement(_reactRouter.Route, { path: '/offers/:offerId', component: _OfferDetail2.default })
	  )
	);

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _react = __webpack_require__(1);

	var _react2 = _interopRequireDefault(_react);

	var _NavLink = __webpack_require__(6);

	var _NavLink2 = _interopRequireDefault(_NavLink);

	var _OfferStore = __webpack_require__(7);

	var _OfferStore2 = _interopRequireDefault(_OfferStore);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = _react2.default.createClass({
	    displayName: 'App',
	    componentWillMount: function componentWillMount() {
	        // OfferStore.init();
	    },
	    render: function render() {
	        return _react2.default.createElement(
	            'div',
	            null,
	            _react2.default.createElement(
	                'ul',
	                { role: 'nav' },
	                _react2.default.createElement(
	                    'li',
	                    null,
	                    _react2.default.createElement(
	                        _NavLink2.default,
	                        { to: '/' },
	                        'Home'
	                    )
	                ),
	                _react2.default.createElement(
	                    'li',
	                    null,
	                    _react2.default.createElement(
	                        _NavLink2.default,
	                        { to: '/offers' },
	                        'Offers'
	                    )
	                ),
	                _react2.default.createElement(
	                    'li',
	                    null,
	                    _react2.default.createElement(
	                        _NavLink2.default,
	                        { to: '/store' },
	                        'Store'
	                    )
	                )
	            ),
	            this.props.children
	        );
	    }
	});

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _react = __webpack_require__(1);

	var _react2 = _interopRequireDefault(_react);

	var _reactRouter = __webpack_require__(3);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = _react2.default.createClass({
		displayName: 'NavLink',
		render: function render() {
			return _react2.default.createElement(_reactRouter.Link, this.props);
		}
	});

/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _geofire = __webpack_require__(8);

	var _geofire2 = _interopRequireDefault(_geofire);

	var _moment = __webpack_require__(9);

	var _moment2 = _interopRequireDefault(_moment);

	var _firebase = __webpack_require__(10);

	var _firebase2 = _interopRequireDefault(_firebase);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var API = 'https://rofr-production-db.firebaseio.com';
	var RADIUS = 200;

	var _offers = {};
	var _initCalled = false;
	var _changeListeners = [];
	var _watchLocationRef = null;

	var _geoQuery = null;
	var _geoFire = null;

	var OfferStore = {
		init: function init() {
			if (_initCalled) return;
			console.log('init');
			var fb = new _firebase2.default(API + '/locations');
			_geoFire = new _geofire2.default(fb);
			_initCalled = true;
			if (navigator.geolocation) {
				// navigator.geolocation.getCurrentPosition(geo_success, geo_error)
				_watchLocationRef = navigator.geolocation.watchPosition(geo_success, geo_error);
			}
		},
		getOffers: function getOffers() {
			var array = [];

			for (var id in _offers) {
				array.push(_offers[id]);
			}return array;
		},
		getOffer: function getOffer(id) {
			return new Promise(function (resolve, reject) {
				if (_offers[id]) {
					resolve(_offers[id]);
				} else {
					var ref = new _firebase2.default(API + '/offers/' + (0, _moment2.default)().format('YYYY-MM-DD') + '/' + id);
					ref.once('value', function (snap) {
						resolve(snap.val());
					});
				}
			});
		},
		getStore: function getStore(name) {
			return new Promise(function (resolve, reject) {
				var ref = new _firebase2.default(API + '/store');
				ref.once('value', function (snap) {
					var stores = snap.val();
					for (var storeId in stores) {
						if (stores[storeId].storeName === name) {
							resolve(stores[storeId]);
							return;
						}
					}
					resolve(null);
				});
			});
		},
		notifyChange: function notifyChange() {
			_changeListeners.forEach(function (listener) {
				listener();
			});
		},
		addChangeListener: function addChangeListener(listener) {
			_changeListeners.push(listener);
		},

		removeChangeListener: function removeChangeListener(listener) {
			if (_watchLocationRef) {
				navigator.geolocation.clearWatch(_watchLocationRef);
			}
			if (_geoQuery) {
				_geoQuery.cancel();
				_geoQuery = null;
			}
			if (_geoFire) {
				_geoFire = null;
			}
			_changeListeners = _changeListeners.filter(function (l) {
				return listener !== l;
			});
			_initCalled = false;
		}
	};

	var geoQueryReady = function geoQueryReady() {
		// console.log("GeoQuery has loaded and fired all other events for initial data");
	};
	var geoQueryKeyEnter = function geoQueryKeyEnter(key, location, distance) {
		// console.log(key + " entered query at " + location + " (" + distance + " km from center)");
		OfferStore.getOffer(key).then(function (offer) {
			if (offer) {
				_offers[key] = offer;
				OfferStore.notifyChange();
			}
		});
	};
	var geo_success = function geo_success(position) {
		if (!_geoQuery) {
			_geoQuery = _geoFire.query({
				center: [position.coords.latitude, position.coords.longitude],
				radius: RADIUS
			});
			_geoQuery.on("ready", geoQueryReady);
			_geoQuery.on("key_entered", geoQueryKeyEnter);
		} else {
			_geoQuery.updateCriteria({
				center: [position.coords.latitude, position.coords.longitude]
			});
		}
	};
	var geo_error = function geo_error() {
		console.log('Location not shared');
	};

	exports.default = OfferStore;

/***/ },
/* 8 */
/***/ function(module, exports) {

	module.exports = require("geofire");

/***/ },
/* 9 */
/***/ function(module, exports) {

	module.exports = require("moment");

/***/ },
/* 10 */
/***/ function(module, exports) {

	module.exports = require("firebase");

/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _react = __webpack_require__(1);

	var _react2 = _interopRequireDefault(_react);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = _react2.default.createClass({
		displayName: 'Home',
		render: function render() {
			return _react2.default.createElement(
				'div',
				null,
				'Home'
			);
		}
	});

/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _react = __webpack_require__(1);

	var _react2 = _interopRequireDefault(_react);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = _react2.default.createClass({
		displayName: 'Store',
		render: function render() {
			return _react2.default.createElement(
				'div',
				null,
				'Store'
			);
		}
	});

/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _react = __webpack_require__(1);

	var _react2 = _interopRequireDefault(_react);

	var _reactRouter = __webpack_require__(3);

	var _OfferStore = __webpack_require__(7);

	var _OfferStore2 = _interopRequireDefault(_OfferStore);

	var _reactDocumentMeta = __webpack_require__(14);

	var _reactDocumentMeta2 = _interopRequireDefault(_reactDocumentMeta);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var styles = {
		products: {
			display: 'flex',
			flexWrap: 'wrap'
		},
		productCard: {
			padding: '2%',
			flexGrow: 1,
			flexBasis: '16%'
		},
		productImage: {
			maxWidth: '100%'
		},
		productInfo: {
			marginTop: 'auto'
		}
	};

	exports.default = _react2.default.createClass({
		displayName: 'Offers',

		contextTypes: {
			router: _react2.default.PropTypes.object.isRequired
		},
		getInitialState: function getInitialState() {
			return {
				offers: [],
				loading: true
			};
		},
		componentWillMount: function componentWillMount() {},
		componentDidMount: function componentDidMount() {
			_OfferStore2.default.init();
			_OfferStore2.default.addChangeListener(this.updateOffers);
		},
		componentWillUnmount: function componentWillUnmount() {
			_OfferStore2.default.removeChangeListener(this.updateOffers);
		},
		updateOffers: function updateOffers() {
			if (!this.isMounted()) return;

			this.setState({
				offers: _OfferStore2.default.getOffers(),
				loading: false
			});
		},
		render: function render() {
			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement(_reactDocumentMeta2.default, { extend: true }),
				_react2.default.createElement(
					'h1',
					null,
					'Offers'
				),
				_react2.default.createElement(
					'div',
					{ style: styles.products },
					this.state.offers.map(function (offer) {
						return _react2.default.createElement(
							_reactRouter.Link,
							{ key: offer.id, to: '/offers/' + offer.id },
							_react2.default.createElement(
								'div',
								{ style: styles.productCard,
									key: offer.id },
								_react2.default.createElement(
									'div',
									null,
									_react2.default.createElement('img', { style: styles.productImage, src: offer.photo })
								)
							)
						);
					})
				),
				this.props.children
			);
		}
	});

/***/ },
/* 14 */
/***/ function(module, exports) {

	module.exports = require("react-document-meta");

/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

	var _react = __webpack_require__(1);

	var _react2 = _interopRequireDefault(_react);

	var _OfferStore = __webpack_require__(7);

	var _OfferStore2 = _interopRequireDefault(_OfferStore);

	var _reactRouter = __webpack_require__(3);

	var _reactDocumentMeta = __webpack_require__(14);

	var _reactDocumentMeta2 = _interopRequireDefault(_reactDocumentMeta);

	var _reactFavicon = __webpack_require__(16);

	var _reactFavicon2 = _interopRequireDefault(_reactFavicon);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = _react2.default.createClass({
	  displayName: 'OfferDetail',

	  contextTypes: {
	    router: _react2.default.PropTypes.object.isRequired
	  },
	  getInitialState: function getInitialState() {
	    return {
	      offer: {}
	    };
	  },
	  componentWillReceiveProps: function componentWillReceiveProps(nextProps) {
	    var _this = this;

	    if (!this.isMounted()) return;
	    console.log('componentWillReceiveProps ' + nextProps.params.offerId);
	    _OfferStore2.default.getOffer(nextProps.params.offerId).then(function (offer) {
	      _this.setState({
	        offer: offer
	      });
	    });
	  },

	  // componentWillMount() {
	  //   console.log('componentWillMount');
	  //   if (!this.props.params.offerId)
	  //     return
	  //   OfferStore.getOffer(this.props.params.offerId).then((offer) => {
	  //     this.setState({
	  //       offer: offer
	  //     }); 
	  //   })
	  // },
	  componentDidMount: function componentDidMount() {
	    var _this2 = this;

	    if (!this.isMounted()) return;
	    console.log('componentDidMount');
	    if (!this.props.params.offerId) return;
	    _OfferStore2.default.getOffer(this.props.params.offerId).then(function (offer) {
	      _this2.setState({
	        offer: offer
	      });
	    });
	  },
	  render: function render() {
	    var meta = {
	      description: this.state.offer.shortDescription,
	      title: this.state.offer.storeName
	    };
	    return _react2.default.createElement(
	      'div',
	      { key: this.state.offer.id },
	      _react2.default.createElement(_reactDocumentMeta2.default, _extends({}, meta, { extend: true })),
	      _react2.default.createElement(
	        'h2',
	        null,
	        'Offer detail 1 of - ',
	        this.state.offer.id
	      ),
	      _react2.default.createElement(
	        'h3',
	        null,
	        this.state.offer.description
	      )
	    );
	  }
	});

/***/ },
/* 16 */
/***/ function(module, exports) {

	module.exports = require("react-favicon");

/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _react = __webpack_require__(1);

	var _react2 = _interopRequireDefault(_react);

	var _reactRouter = __webpack_require__(3);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = _react2.default.createClass({
		displayName: 'Cert',

		contextTypes: {
			router: _react2.default.PropTypes.object.isRequired
		},
		render: function render() {
			return _react2.default.createElement(
				'div',
				null,
				this.props.params.id
			);
		}
	});

/***/ },
/* 18 */
/***/ function(module, exports) {

	module.exports = require("express");

/***/ },
/* 19 */
/***/ function(module, exports) {

	module.exports = require("path");

/***/ },
/* 20 */
/***/ function(module, exports) {

	module.exports = require("compression");

/***/ },
/* 21 */
/***/ function(module, exports) {

	module.exports = require("serve-favicon");

/***/ }
/******/ ]);