var express = require('express')
var path = require('path')
var compression = require('compression')
import React from 'react'
import {renderToString} from 'react-dom/server'
import {match, RouterContext} from 'react-router'
import routes from './components/routes'
import OfferStore from './stores/OfferStore'
var favicon = require('serve-favicon');

var app = express()
app.use(compression())
// serve our static stuff like index.css
app.use(express.static(path.join(__dirname, 'node_modules/sw-toolbox')));
app.use(express.static(path.join(__dirname, 'public')))

// app.get('/.well-known/acme-challenge/:id', function(req, res) {
//   res.send('ghV2hX0mn3xtOHpxs29a3G6hVRerNsBqA5HZYSMAjw4.d3amT42RzK2pDf4bzjhqCuhvWGRJCkXPmb3yuDRP-po');
// });
// send all requests to index.html so browserHistory in React Router works
app.get('*', (req, res) => {
  console.log('start')
  match({ routes: routes, location: req.url }, (err, redirect, props) => {
    // in here we can make some decisions all at once
    if (err) {
      // there was an error somewhere during route matching
      res.status(500).send(err.message)
    } else if (redirect) {
      console.log('redirect')
      // we haven't talked about `onEnter` hooks on routes, but before a
      // route is entered, it can redirect. Here we handle on the server.
      res.redirect(redirect.pathname + redirect.search)
    } else if (props) {
      console.log(props.params)
      if (props.params.offerId) {
        OfferStore.getOffer(props.params.offerId).then((offer) => {
          if (!offer) {
              res.status(404).send('Not Found')
          } else {
            // app.use(favicon(offer.storeLogo));
            // if we got props then we matched a route and can render
            const appHtml = renderToString(<RouterContext {...props}/>)
            res.send(renderPage(appHtml, offer))    
          }
          
        })
        
      }
      else if (props.params.storeName) {
        OfferStore.getStore(props.params.storeName).then((store) => {
          if (!store) {
              res.status(404).send('Not Found')
          } else {
            // app.use(favicon(offer.storeLogo));
            // if we got props then we matched a route and can render
            const appHtml = renderToString(<RouterContext {...props}/>)
            res.send(renderStorePage(appHtml, store))    
          }
          
        })
      } else {
        // if we got props then we matched a route and can render
        const appHtml = renderToString(<RouterContext {...props}/>)
        res.send(renderPage(appHtml, req.query.description))
      }
      
    } else {
      //res.send(renderBlankPage())    
      // no errors, no redirect, we just didn't match anything
      res.status(404).send('Not Found')
    }
  })
})
function renderBlankPage() {
  return `
    <!DOCTYPE html>
      <html>
      <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>For cert</title>
      </head>
      <body>
      </body>
      </html>`
}
function renderPage(appHtml, {offerDescription='Rofr',storeName='Rofr', storeLogo='favicon.ico'}) {
	return `
		<!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name='description' content='${offerDescription}'>
    <meta name='storeName' content='${storeName}'>
    <meta name='type' content='offer'>
    <title>${storeName}</title>
    <link rel="shortcut icon" href='${storeLogo}'>
  </head>
  <body>
    <script src="/app.js"></script>
  </body>
  </html>`
}

function renderStorePage(appHtml, {storeMetaDescription='Rofr',storeMetaTitle='Rofr', storeName='', storeType='Store'}) {
  return `
    <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name='description' content='${storeMetaDescription}'>
    <meta name='storeName' content='${storeName}'>
    <meta name='type' content='store'>
    <meta name='storeType' content='${storeType}'>
    <title>${storeMetaTitle}</title>
    <link rel="shortcut icon" href='favicon.ico'>
  </head>
  <body>
    <script src="/app.js"></script>
  </body>
  </html>`
}
  
var PORT = process.env.PORT || 8080
app.listen(PORT, function() {
  console.log('Production Express server running at localhost:' + PORT)
})